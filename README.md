# Krakeno Monitoring stack

## Softwares

* [prometheus-adapter](https://github.com/helm/charts/tree/master/stable/prometheus-adapter)
* [prometheus-operator](https://github.com/helm/charts/tree/master/stable/prometheus-operator)
* [celery-exporter](https://github.com/OvalMoney/celery-exporter)


## USEFUL LINKS

* [Deeper explanation on custom/external metrics](https://learnk8s.io/autoscaling-apps-kubernetes)
* [Autoscaling on metrics not related to Kubernetes objects](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale-walkthrough/#autoscaling-on-metrics-not-related-to-kubernetes-objects)
* [Autoscaling Deployments with different custom/external metrics](https://cloud.google.com/kubernetes-engine/docs/tutorials/autoscaling-metrics#pubsub_8)
* [How to auto-scale celery pods](https://stackoverflow.com/questions/58058192/how-to-auto-scale-kubernetes-pods-based-on-number-of-tasks-in-celery-task-queue)
* [Learn How to Assign Pods to Nodes in Kubernetes Using nodeSelector and Affinity Features](https://medium.com/kubernetes-tutorials/learn-how-to-assign-pods-to-nodes-in-kubernetes-using-nodeselector-and-affinity-features-e62c437f3cf8)


## Why prometheus-adapter 5.0
* [Metrics labelSelector not filtering external metrics](https://github.com/DirectXMan12/k8s-prometheus-adapter/issues/255)
* [0.6.0 External Metrics Bug around namespace being appended](https://github.com/DirectXMan12/k8s-prometheus-adapter/issues/282)
