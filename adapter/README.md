# Prometheus Adapter

It is used ton adapt metrics exporter by [Celery Prometheus Exporter](https://gitlab.com/bodzen/krakeno/forked-celery-prometheus-exporter), which in turn is used by [Content-carrier](https://gitlab.com/bodzen/krakeno/content-carrier)'s Horizontal pod autoscaler.


## Get metrics

```
  kubectl get --raw /apis/custom.metrics.k8s.io/v1beta1

  kubectl get --raw /apis/external.metrics.k8s.io/v1beta1
```

### Expected result for external metrics
```
{"kind":"ExternalMetricValueList",
 "apiVersion":"external.metrics.k8s.io/v1beta1",
 "metadata":{"selfLink":"/apis/external.metrics.k8s.io/v1beta1/namespaces/omega/ccarrier_nb_pending_tasks"},"items":
 [{"metricName":"celery_tasks_by_name",
   "metricLabels":{"__name__":"celery_tasks_by_name",
                   "instance":"celery-prometheus-exporter:9540",
                   "job":"celery",
                   "name":"content_carrier.carrier.content_carrier_task",
                   "state":"RECEIVED"},"timestamp":"2020-10-08T12:59:41Z",
   "value":"0"},
  {"metricName":"celery_tasks_by_name",
   "metricLabels":{"__name__":"celery_tasks_by_name",
                   "instance":"celery-prometheus-exporter:9540",
                   "job":"celery",
                   "name":"content_carrier.carrier.content_carrier_task",
                   "state":"STARTED"},"timestamp":"2020-10-08T12:59:41Z",
   "value":"0"},
  {"metricName":"celery_tasks_by_name",
   "metricLabels":{"__name__":"celery_tasks_by_name",
                   "instance":"celery-prometheus-exporter:9540",
                   "job":"celery",
                   "name":"content_carrier.carrier.content_carrier_task",
                   "state":"SUCCESS"},"timestamp":"2020-10-08T12:59:41Z",
   "value":"2232"},
  {"metricName":"celery_tasks_by_name",
   "metricLabels":{"__name__":"celery_tasks_by_name",
                   "instance":"celery-prometheus-exporter:9540",
                   "job":"celery",
                   "name":"content_carrier.carrier.content_carrier_task",
                   "state":"FAILURE"},"timestamp":"2020-10-08T12:59:41Z",
   "value":"35"},
  {"metricName":"celery_tasks_by_name",
   "metricLabels":{"__name__":"celery_tasks_by_name",
                   "instance":"celery-prometheus-exporter:9540",
                   "job":"celery",
                   "name":"content_carrier.carrier.content_carrier_task",
                   "state":"PENDING"},"timestamp":"2020-10-08T12:59:41Z",
   "value":"0"},
  {"metricName":"celery_tasks_by_name",
   "metricLabels":{"__name__":"celery_tasks_by_name",
                   "instance":"celery-prometheus-exporter:9540",
                   "job":"celery",
                   "name":"content_carrier.carrier.content_carrier_task",
                   "state":"RETRY"},"timestamp":"2020-10-08T12:59:41Z",
   "value":"0"},
  {"metricName":"celery_tasks_by_name",
   "metricLabels":{"__name__":"celery_tasks_by_name",
                   "instance":"celery-prometheus-exporter:9540",
                   "job":"celery",
                   "name":"content_carrier.carrier.content_carrier_task",
                   "state":"REVOKED"},"timestamp":"2020-10-08T12:59:41Z",
   "value":"0"}
 ]}
```
